import React from "react";
import { Route, Routes } from "react-router-dom";
import Navbar from "./Components/Navbar";
import About from "./Pages/About";
import Home from "./Pages/Home";
import News from "./Pages/News";


const Routers = () => {
  return (
    <>
    <Navbar/>
    <Routes>
            <Route exact path='/' element={<Home/>} />
            <Route path='/about' element={<About/>} />
            <Route path='/news' element={<News/>} />
    </Routes>
    </>
  );
};

export default Routers;
