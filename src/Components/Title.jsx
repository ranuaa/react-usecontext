import { Box, Typography } from "@mui/material";
import React, { useContext } from "react";
import { CommonProvider } from "../Context/CommonContext";

const Title = () => {
    const context = useContext(CommonProvider)
    const halaman = context.page
    console.log(halaman)
  return (
    <Box>
        <Typography variant="h3" align='center' sx={{marginTop: '10px'}}>
          {halaman === 1 ? (context.uiState.title1).toUpperCase() : halaman === 2 ? (context.uiState.title2).toUpperCase() : (context.uiState.title3).toUpperCase()}
        </Typography>
    </Box>
  );
};

export default Title;
