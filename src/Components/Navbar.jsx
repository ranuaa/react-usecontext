import React, { useContext } from "react";
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import Button from '@mui/material/Button';
import IconButton from '@mui/material/IconButton';
import MenuIcon from '@mui/icons-material/Menu';
import { NavLink } from "react-router-dom";
import { CommonProvider } from "../Context/CommonContext";
import { Input, TextField } from "@mui/material";
import { useState } from "react";
import Modal from '@mui/material/Modal'

const style = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 400,
    bgcolor: 'background.paper',
    border: '2px solid #000',
    boxShadow: 24,
    p: 4,
  };


const Navbar = () => {
    const context = useContext(CommonProvider)
    const halaman = context.page
    const [color, setColor] = useState("")
    const [title, setTitle] = useState("")
    const [userName, setUserName] = useState('')
    const [password, setPassword] = useState('')
    const handleLogin = () => {
        if(userName.toLowerCase() === "admin" && password.toLowerCase() === "password"){
            context.setLogedIn(true)
            setUserName('')
            setPassword('')
            handleClose()
        }else{
            alert("Wrong Password Or UserName")
        }
    }
    console.log(context)
    const handleSubmit = () => {
        if(halaman === 1){
            context.updatesUiState({
                color1 : color,
                title1 : title,
            })
            console.log("ini halaman 1")
        }else if(halaman === 2){
            context.updatesUiState({
                color2 : color,
                title2 : title,
            })
            console.log("ini halaman 2")
        }else{
            context.updatesUiState({
                color3 : color,
                title3 : title,
            })
            console.log("ini halaman 3")
        }
        setColor('')
        setTitle('')
    }
    const handleLogOut = () => {
        context.setLogedIn(false)
    }
    const [open, setOpen] = useState(false);
    const handleOpen = () => {
        if(context.logedIn){
            handleLogOut()
        }else{
            setOpen(true)
        }
    };
    const handleClose = () => setOpen(false);
  return (
    <Box sx={{ flexGrow: 1 }}>
    <AppBar position="static" sx={{backgroundColor: context.page === 1 ? context.uiState.color1 : context.page === 2 ? context.uiState.color2 : context.uiState.color3}}>
      <Toolbar>
        <Box component="div" sx={{ flexGrow: 1, display: 'flex', gap: '15px',  alignItems: 'center' }}>
        <NavLink onClick={() => context.updatePage(1)} exact to={{ pathname: '/'}} style={{textDecoration: 'none'}} activeStyle={{borderBottom: '2px solid white',color: 'white'}}>
        <Typography variant="h6"  >
          Home
        </Typography>
        </NavLink>

        <NavLink onClick={() => context.updatePage(2)} to={{pathname: '/about'}} style={{textDecoration: 'none'}} activeStyle={{borderBottom: '2px solid white', color: 'white'}}>
        <Typography variant="h6"  >
          About
        </Typography>
        </NavLink>

        <NavLink onClick={() => context.updatePage(3)} to={{pathname: '/news'}} style={{textDecoration: 'none'}} activeStyle={{borderBottom: '2px solid white', color: 'white'}}>
        <Typography variant="h6"  >
          News
        </Typography>
        </NavLink>
        <Box style={{display:'flex', justifyContent: 'center', alignItems: 'center', gap: '20px'}}>
            <Box style={{display:'flex', justifyContent: 'center', alignItems: 'center', gap: '5px'}}>
            <TextField value={color} onChange={(e) => setColor(e.target.value)} id="filled-basic" label="Change Color (use #hex)" variant="filled" size="small"  margin="dense"/>
            <TextField value={title} onChange={(e) => setTitle(e.target.value)} id="filled-basic" label="Change title" variant="filled" size="small"  margin="dense"/>
            <Button onClick={handleSubmit} style={{backgroundColor: 'white', color: 'black'}}>Go</Button>
            </Box>
        </Box>
        </Box>
        <Typography variant="h6" sx={{marginRight: '10px'}} >
        {context.logedIn ? "Hi admin" : ''}
        </Typography>
        <Button onClick={handleOpen} sx={{backgroundColor: 'blue', color: 'white'}}>{context.logedIn ? "Log out" : 'Login'}</Button>
      </Toolbar>
      <Modal
        open={open}
        onClose={handleClose}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Box sx={style}>
          <Typography id="modal-modal-title" variant="h6" component="h2" align="center">
            Login
          </Typography>
          <Typography id="modal-modal-title" variant="p" component="p" align="center">
            username : admin  password : password
          </Typography>
          <Box style={{display:'flex', justifyContent: 'center', alignItems: 'center', gap: '20px'}}>
            <Box style={{display:'flex', justifyContent: 'center', alignItems: 'center', gap: '5px'}}>
            <TextField value={userName} onChange={(e) => setUserName(e.target.value)} id="filled-basic" label="UserName" variant="filled" size="small"  margin="dense"/>
            <TextField value={password} onChange={(e) => setPassword(e.target.value)} id="filled-basic" label="Password" variant="filled" size="small"  margin="dense"/>
            <Button onClick={handleLogin} style={{backgroundColor: 'green', color: 'white'}}>Login</Button>
            </Box>
        </Box>
        </Box>
      </Modal>
    </AppBar>
  </Box>
  );
};

export default Navbar;
