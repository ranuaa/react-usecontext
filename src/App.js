import { createTheme, ThemeProvider } from "@mui/material";
import { useContext } from "react";
import Navbar from "./Components/Navbar";
import { CommonProvider, ContextProvider } from "./Context/CommonContext";
import Routers from './Routers'


function App() {
  const context = useContext(CommonProvider)
  console.log(context)
  const theme = createTheme({
    palette: {
      primary: {
        main: '#11cb5f'
      },
      secondary: {
        main: '#11cb61'
      },
      custom: {
        main: '#f57c00'
      }
    },
  });
  return (
    <div className="App">
      <ContextProvider>
      <ThemeProvider theme={theme}>
      <Routers/>
      </ThemeProvider>
      </ContextProvider>
    </div>
  );
}

export default App;
