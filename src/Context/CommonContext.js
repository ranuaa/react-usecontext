import React, { createContext, useState } from "react";

export const CommonProvider = createContext()

export const ContextProvider = ({children}) => {

    const [page,setPage] = useState(1)
    const [uiState, setUiState] = useState(
      {
        color1: '11cb5f', 
        title1: 'Title Goes Here!',
        color2: '11cb5f', 
        title2: 'Title Goes Here!',
        color3: '11cb5f', 
        title3: 'Title Goes Here!',
      })
    const [logedIn, setLogedIn]= useState(false)
    const updatePage = (page) => {
      setPage(page)
    }
    const updatesUiState = (updatedState) => {
        setUiState({...uiState, ...updatedState});
    }

  return (
    <CommonProvider.Provider value={{uiState, updatesUiState, updatePage, page, setLogedIn, logedIn}}>
        {children}
    </CommonProvider.Provider>
  );
};

