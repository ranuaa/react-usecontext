import React, { useContext, useEffect }  from "react";
import Title from "../Components/Title";
import { CommonProvider } from "../Context/CommonContext";

const Home = () => {
  const context = useContext(CommonProvider)
  return (
  <div>
    <Title/>
  </div>
  );
};

export default Home;
